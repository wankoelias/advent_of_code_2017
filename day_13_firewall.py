walls = [map(int, line.split(": ")) for line in open("input_day_13")]

delay = 0
caught = True

while caught:

    caught = False

    for depth, rang in walls:
        pos = (depth + delay) % (2*rang - 2)

        if pos == 0:
            caught = True
            break

    if not caught:
        print delay

    delay += 1