p_input = 347991


# PART 1

total, level = 1, 1

while total < p_input:
    level += 2
    total = total + level*4 - 4 #level ** 2

offset = total - p_input
steps = offset % (level - 1)

print (level - 1) / 2 + abs((level / 2) - steps)

# PART 2

# x, y, value
values = [(0, 0, 1)]

directions = [(0, 1), (-1, 0), (0, -1), (1, 0)]

level = 1
x, y = 0, 0

terminate = False

while not terminate:

    for direction in range(4):

        if terminate:
            break

        dirX, dirY = directions[direction]

        moveN = level

        if direction == 0:
            moveN = level - 2
        elif direction in [1, 2]:
            moveN = level - 1

        for _ in range(moveN):

            x += dirX
            y += dirY

            new = sum([k[2] for k in values if abs(x-k[0]) <= 1 and abs(y-k[1]) <= 1])
            values.append((x, y, new))

            if new >= p_input:
                print new

                terminate = True
                break

    level += 2