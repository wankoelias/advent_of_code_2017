inp = "230,1,2,221,97,252,168,169,57,99,0,254,181,255,235,167"

inp_ = map(ord, list(inp)) + [17, 31, 73, 47, 23]
size = 256

knot = range(size)
pos = 0
skip = 0

# create list

for _ in range(64):
    for length in inp_:

        p1 = knot[pos:pos + length]
        p2 = knot[:max(0, (pos+length) - size)]

        select = p1 + p2
        select.reverse()

        p1_r = select[:len(p1)]
        p2_r = select[len(p1):]

        knot[pos:pos + length] = p1_r
        knot[:max(0, (pos + length) - size)] = p2_r

        pos = (pos + skip + length) % size
        skip += 1

# compute hash

hash = ""

for start in range(0, size, 16):
    block = map(str, knot[start:start+16])
    block_hex = hex(eval("^".join(block)))[2:]
    block_hex = "0" + block_hex if len(block_hex) == 1 else block_hex
    hash = hash + block_hex

print hash
