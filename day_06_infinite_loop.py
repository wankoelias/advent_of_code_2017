inp = "14	0	15	12	11	11	3	5	1	6	8	4	9	1	8	4"

# Part 1

# current = map(int, inp.split())
#
# states = []
# moves = 0
#
# while tuple(current) not in states:
#     states.append(tuple(current))
#
#     moves += 1
#
#     index, value = [(i, x) for i, x in enumerate(current) if x == max(current)][0]
#
#     current[index] = 0
#
#     while value > 0:
#         index = (index + 1) % len(current)
#         current[index] += 1
#         value -= 1
#
# print moves

# Part 1 and Part 2

current = map(int, inp.split())

states = []
moves = 0
status = 0
reference = None

while status < 2:

    # Part 2
    if reference == tuple(current):
        status = 2
        print moves

    # part 1
    if tuple(current) in states and status == 0:
        status = 1
        print moves
        moves = 0
        reference = tuple(current)


    states.append(tuple(current))

    moves += 1

    index, value = [(i, x) for i, x in enumerate(current) if x == max(current)][0]

    current[index] = 0

    while value > 0:
        index = (index + 1) % len(current)
        current[index] += 1
        value -= 1