data = map(int, open("input_day_05").read().split("\n"))


pos = 0
count = 0

while pos < len(data):

    old = pos
    pos = pos + data[pos]

    if data[old] < -3000:
        data[old] += 1
    else:
        data[old] -= 1
    count += 1

print count