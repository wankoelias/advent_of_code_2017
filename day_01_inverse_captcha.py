input = open("input_day_01").read()

sum = 0


for i, digit in enumerate(input):

    # if i+1 == len(input):
    #     next = input[0]
    # else:
    #     next = input[i+1]

    ### day 2
    nexti = (i+len(input)/2) % len(input)

    next = input[nexti]

    if next == digit:
        sum += int(digit)

print sum