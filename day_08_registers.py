
registers = {}
highestEver = 0
for line in open("input_day_08").readlines():
    split = line.split()

    if not split[0] in registers.keys():
        registers[split[0]] = 0

    if not split[4] in registers.keys():
        registers[split[4]] = 0

    if eval("registers['{}']{}{}".format(split[4], split[5], split[6])):
        if split[1] == "inc":
            registers[split[0]] += int(split[2])

            if registers[split[0]] > highestEver:
                highestEver = registers[split[0]]

        else:
            registers[split[0]] -= int(split[2])


print max(registers.values())
print highestEver
