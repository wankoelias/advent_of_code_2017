import re
from collections import Counter
inp = open("day_07_input")

# parse nodes and find root
nodes = []
root = set()
sons = set()


for line in inp.readlines():
    parts = re.findall("(\w+)+", line)

    nodes.append(parts)
    root.add(parts[0])
    sons = sons.union(set(parts[2:]))


root = (root - sons).pop()

print "root: ", root

def buildNode(son):
    for node in nodes:
        if son == node[0]:
            nodes.remove(node)
            return {"n": node[0], "w": int(node[1]), "s": [buildNode(s) for s in node[2:]]}

def printNode(node, level=0):
    print "\t"*level + node["n"], node["w"]
    for s in node["s"]:
        printNode(s, level+1)

def findImbalance(node, level=0):

    weights = [[findImbalance(s, level + 1), s["n"]] for s in node["s"]]
    weights_v = Counter([x[0] for x in weights])


    if len(weights_v) > 1:
        falseName = [x[1] for x in weights if weights_v[x[0]] == 1][0]
        wrongWeight = [x["w"] for x in node["s"] if x["n"] == falseName][0]

        print "wrong weight in level", level, "for disk:", falseName, wrongWeight
        print "right weight: ", weights_v.most_common()[0][0] - weights_v.most_common()[1][0] + wrongWeight

    return sum([w[0] for w in weights]) + node["w"]




# build tree

tree = buildNode(root)

# printNode(tree)

findImbalance(tree)