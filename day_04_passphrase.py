valid = 0

# part 1
for p in open("input_day_04").readlines():
    p_split = p.split()
    if len(p_split) == len(set(p_split)):
        valid += 1

print valid

# part 2
from collections import Counter

valid = 0
for p in open("input_day_04").readlines():
    p_counter = [Counter(x) for x in p.split()]
    if not [x for x in p_counter if len([k for k in p_counter if k == x]) > 1]:
        valid += 1

print valid

