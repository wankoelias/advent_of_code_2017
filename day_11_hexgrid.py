inp = open("input_day_11").read()
inp_ = inp.split(",")

x = 0
y = 0

directions = {
    "n": (0, 1),
    "ne": (1, 0.5),
    "se": (1, -0.5),
    "s": (0, -1),
    "sw": (-1, -0.5),
    "nw": (-1, 0.5),
}

farthest = 0

for i in inp_:

    dx, dy = directions[i]
    x += dx
    y += dy

    d = abs(y) - abs(x)/2.0 + abs(x)

    if d > farthest:
        farthest = d

print abs(y) - abs(x)/2.0 + abs(x)
print farthest



