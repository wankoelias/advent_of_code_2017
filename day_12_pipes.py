import re

groups = []

for line in open("input_day_12").readlines():
    g_member = set(re.findall("\d+", line))

    g_inter = [x for x in groups if x.intersection(g_member)]

    for g in g_inter:
        groups.remove(g)

    groups.append(set.union(g_member, *g_inter))

# part 1
print len([g for g in groups if "0" in g][0])

# part 2
print len(groups)