import re

raw = open("input_day_09").read()

# cancel characters

canceled = re.sub("!.", "", raw)

# delete garbage

clean = re.sub("<.*?>", "", canceled).replace(",", "")


# calculate score

level = 0
score = 0

for c in clean:
    if c == "{":
        level += 1
        score += level
    if c == "}":
        level -= 1

print score

# part 2

print sum(map(len, re.findall("<(.*?)>", canceled)))
