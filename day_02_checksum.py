input_ = open("input_day_02").read()

sum([max(r) - min(r) for r in [map(int, row.split("\t")) for row in input_.split("\n") if row]])

inp = [map(int, row.split("\t")) for row in input_.split("\n") if row]


summe = 0

for r in inp:
    # loop through rows

    print r

    for i, number in enumerate(r):
        # loop through digits of a row

        matches = []

        for i_, number_ in enumerate(r):
            if i_ == i:
                continue

            if number % number_ == 0:
                matches.append(number/number_)

        summe += sum(matches)

print summe